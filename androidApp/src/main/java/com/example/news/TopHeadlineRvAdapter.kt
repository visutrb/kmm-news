package com.example.news

import android.text.format.DateFormat
import android.text.format.DateUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.kmmnews.shared.entity.Article
import com.example.news.databinding.ViewHeadlineBinding
import com.example.news.databinding.ViewTopHeadlineBinding
import java.text.SimpleDateFormat
import java.util.*

class TopHeadlineRvAdapter : RecyclerView.Adapter<TopHeadlineRvAdapter.ViewHolder>() {

    var topHeadlines: List<Article> = listOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun getItemCount(): Int {
        return topHeadlines.count()
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == 0) ITEM_TYPE_TOP_HEADLINE else ITEM_TYPE_HEADLINE
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        var viewHolder: ViewHolder
        if (viewType == ITEM_TYPE_TOP_HEADLINE) {
            val binding = ViewTopHeadlineBinding.inflate(inflater, parent, false)
            viewHolder = ViewHolder(binding.root)
            viewHolder.imgHeadline = binding.imgHeadline
            viewHolder.tvTitle = binding.tvTitle
            viewHolder.tvPublishDate = binding.tvPublishDate
        } else {
            val binding = ViewHeadlineBinding.inflate(inflater, parent, false)
            viewHolder = ViewHolder(binding.root)
            viewHolder.imgHeadline = binding.imgHeadline
            viewHolder.tvTitle = binding.tvTitle
            viewHolder.tvPublishDate = binding.tvPublishDate
        }
        return viewHolder
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val article = topHeadlines[position]
        holder.bind(article)
    }

    class ViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {

        lateinit var imgHeadline: ImageView
        lateinit var tvTitle: TextView
        lateinit var tvPublishDate: TextView

        fun bind(article: Article) {
            tvTitle.text = article.title

            article.publishedAt?.let {
                val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
                val now = Date()
                val date = dateFormat.parse(it)!!
                val dateDiff = now.time - date.time
                val isToday = DateUtils.isToday(date.time)
                Log.d("TopHeadlineRvAdapter", "dateDiff = $dateDiff")
                if (isToday && dateDiff <= 3600000) {
                    tvPublishDate.text = DateUtils.getRelativeTimeSpanString(
                        date.time,
                        now.time,
                        10
                    )
                } else if (isToday) {
                    val systemTimeFormat = DateFormat.getTimeFormat(itemView.context)
                    tvPublishDate.text = systemTimeFormat.format(date)
                } else {
                    val systemDateFormat = DateFormat.getMediumDateFormat(itemView.context)
                    tvPublishDate.text = systemDateFormat.format(date)
                }
            }
            article.urlToImage?.let {
                Glide.with(imgHeadline)
                    .load(it)
                    .into(imgHeadline)
            }
        }
    }

    companion object {
        private const val ITEM_TYPE_TOP_HEADLINE = 1
        private const val ITEM_TYPE_HEADLINE = 2
    }

}
