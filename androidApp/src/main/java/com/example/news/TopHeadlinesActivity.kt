package com.example.news

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.kmmnews.shared.entity.ArticlesResponse
import com.example.kmmnews.shared.presentation.topheadlines.TopHeadlinesPresenter
import com.example.kmmnews.shared.presentation.topheadlines.TopHeadlinesView
import com.example.news.databinding.ActivityTopheadlineBinding

class TopHeadlinesActivity : AppCompatActivity(), TopHeadlinesView {

    private val presenter = TopHeadlinesPresenter()

    private lateinit var binding: ActivityTopheadlineBinding
    private lateinit var adapter: TopHeadlineRvAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTopheadlineBinding.inflate(layoutInflater)
        setContentView(binding.root)

        adapter = TopHeadlineRvAdapter()

        binding.swipeRefresh.setOnRefreshListener {
            presenter.loadTopHeadlines()
        }

        binding.recyclerView.let {
            it.layoutManager = LinearLayoutManager(this)
            it.addItemDecoration(DividerItemDecoration(this, LinearLayoutManager.VERTICAL))
            it.adapter = adapter
        }

        presenter.take(view = this)
        presenter.loadTopHeadlines()
    }

    override fun onLoadingStarted() {
        binding.swipeRefresh.isRefreshing = true
        Log.d(TAG, "Loading started")
    }

    override fun onLoadingFailed(error: Throwable?) {
        binding.swipeRefresh.isRefreshing = false
        Log.e(TAG, "Loading failed", error)
    }

    override fun onLoadingFinished(articlesResponse: ArticlesResponse) {
        binding.swipeRefresh.isRefreshing = false
        Log.d(TAG, "Loading finished, $articlesResponse")
        adapter.topHeadlines = articlesResponse.articles
    }

    companion object {
        private const val TAG = "MainActivity"
    }
}