//
//  Date+Formatter.swift
//  News
//
//  Created by Visutr Boonnateephisit on 14/6/21.
//

import Foundation

extension Date {
    
    func toDisplayString() -> String {
        let now = Date()
        let diff = self.timeIntervalSince(now)
        
        let calendar = Calendar.current
        let isToday = calendar.isDateInToday(self)
        
        let formatter = DateFormatter()
        
        if isToday && diff > -10900 {
            let relativeFormatter = RelativeDateTimeFormatter()
            return relativeFormatter.localizedString(for: self, relativeTo: now)
        } else if isToday {
            formatter.dateStyle = .none
            formatter.timeStyle = .short
        } else {
            formatter.dateStyle = .medium
            formatter.timeStyle = .short
        }
        return formatter.string(from: self)
    }
    
}
