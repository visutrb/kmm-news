//
//  ViewController.swift
//  News
//
//  Created by Visutr Boonnateephisit on 14/6/21.
//

import UIKit
import Shared
import os

class TopHeadlinesViewController: UIViewController {
    
    private let presenter = TopHeadlinesPresenter()
    private var headlines: [Article] = []
    
    private var refreshControl: UIRefreshControl!
    private var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.topItem?.title = "News"
        
        initSubviews()
        initConstraints()
        
        presenter.take(view: self)
        presenter.loadTopHeadlines()
    }
    
    private func initSubviews() {
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(onRefresh(sender:)), for: .valueChanged)
        
        tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.dataSource = self
        tableView.addSubview(refreshControl)
        view.addSubview(tableView)
    }
    
    private func initConstraints() {
        NSLayoutConstraint.activate([
            tableView.leftAnchor.constraint(equalTo: view.leftAnchor),
            tableView.rightAnchor.constraint(equalTo: view.rightAnchor),
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
    
    @objc private func onRefresh(sender: UIRefreshControl) {
        presenter.loadTopHeadlines()
    }
}

extension TopHeadlinesViewController: TopHeadlinesView {
    
    func onLoadingStarted() {
        refreshControl.beginRefreshing()
        NSLog("Loading started")
    }

    func onLoadingFailed(error: KotlinThrowable?) {
        refreshControl.endRefreshing()
        NSLog("Loading failed. \(String(describing: error?.description()))")
    }
    
    func onLoadingFinished(articlesResponse: ArticlesResponse) {
        refreshControl.endRefreshing()
        NSLog("Loading finished")
        headlines = articlesResponse.articles
        tableView.reloadData()
    }
}

extension TopHeadlinesViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return headlines.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let row = indexPath.row
        let headline = headlines[row]
        let identifier = row == 0 ? "TopHeadline" :"Headline"
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? HeadlineTableViewCell ?? ((row == 0) ? TopHeadlineTableViewCell() : HeadlineTableViewCell())
            
        cell.headline = headline
        return cell
    }
}

