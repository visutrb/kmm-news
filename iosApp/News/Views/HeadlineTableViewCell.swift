//
//  HeadlineTableViewCell.swift
//  News
//
//  Created by Visutr Boonnateephisit on 14/6/21.
//

import UIKit
import Shared
import Kingfisher

class HeadlineTableViewCell: UITableViewCell {
    
    var headline: Article! {
        didSet { updateSubviews() }
    }
    
    var headlineImageView: UIImageView!
    var titleLabel: UILabel!
    var publishDateLabel: UILabel!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier:  reuseIdentifier)
        initSubviews()
        initConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder: ) has not been implemented")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func initSubviews() {
        headlineImageView = UIImageView()
        headlineImageView.translatesAutoresizingMaskIntoConstraints = false
        headlineImageView.contentMode = .scaleAspectFill
        headlineImageView.clipsToBounds = true
        headlineImageView.layer.cornerRadius = 10
        headlineImageView.backgroundColor = .lightGray
        contentView.addSubview(headlineImageView)
        
        titleLabel = UILabel()
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.numberOfLines = 0
        titleLabel.font = UIFont.systemFont(ofSize: 16, weight: .bold)
        contentView.addSubview(titleLabel)
        
        publishDateLabel = UILabel()
        publishDateLabel.translatesAutoresizingMaskIntoConstraints = false
        publishDateLabel.font = UIFont.systemFont(ofSize: 12)
        contentView.addSubview(publishDateLabel)
    }
    
    func initConstraints() {
        NSLayoutConstraint.activate([
            headlineImageView.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 10),
            headlineImageView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
            headlineImageView.bottomAnchor.constraint(lessThanOrEqualTo: contentView.bottomAnchor, constant: -10),
            headlineImageView.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: 0.3),
            headlineImageView.heightAnchor.constraint(equalTo: headlineImageView.widthAnchor, multiplier: 1),
            
            titleLabel.leftAnchor.constraint(equalTo: headlineImageView.rightAnchor, constant: 10),
            titleLabel.rightAnchor.constraint(lessThanOrEqualTo: contentView.rightAnchor, constant: -10),
            titleLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
            titleLabel.bottomAnchor.constraint(lessThanOrEqualTo: contentView.bottomAnchor, constant: -10),
            
            publishDateLabel.leftAnchor.constraint(equalTo: titleLabel.leftAnchor),
            publishDateLabel.rightAnchor.constraint(lessThanOrEqualTo: contentView.rightAnchor, constant: -10),
            publishDateLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 10),
            publishDateLabel.bottomAnchor.constraint(lessThanOrEqualTo: contentView.bottomAnchor, constant: -10)
        ])
    }
    
    private func updateSubviews() {
        titleLabel.text = headline.title
       
        
        if let publishedAt = headline.publishedAt {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
            
            let date = dateFormatter.date(from: publishedAt)
            publishDateLabel.text = date?.toDisplayString()
        }
        
        if let urlToImage = headline.urlToImage {
            let url = URL(string: urlToImage)
            headlineImageView.kf.setImage(with: url)
        }
    }
    
    
}
