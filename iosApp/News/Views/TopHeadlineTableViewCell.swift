//
//  TopHeadlineTableViewCell.swift
//  News
//
//  Created by Visutr Boonnateephisit on 14/6/21.
//

import UIKit
import Shared

class TopHeadlineTableViewCell: HeadlineTableViewCell {

    override func initConstraints() {
        NSLayoutConstraint.activate([
            headlineImageView.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 10),
            headlineImageView.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -10),
            headlineImageView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
            headlineImageView.widthAnchor.constraint(equalTo: contentView.widthAnchor, constant: -20),
            headlineImageView.heightAnchor.constraint(equalTo: headlineImageView.widthAnchor, multiplier: 9 / 16),
            
            titleLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 10),
            titleLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -10),
            titleLabel.topAnchor.constraint(equalTo: headlineImageView.bottomAnchor, constant: 10),
            
            publishDateLabel.leftAnchor.constraint(equalTo: titleLabel.leftAnchor),
            publishDateLabel.rightAnchor.constraint(equalTo: titleLabel.rightAnchor),
            publishDateLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 10),
            publishDateLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10)
        ])
    }
    
}
