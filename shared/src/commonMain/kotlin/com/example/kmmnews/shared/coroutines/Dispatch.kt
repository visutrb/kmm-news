package com.example.kmmnews.shared.coroutines

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

fun CoroutineScope.dispatch(block: suspend () -> Unit) {
    launch { block() }
}