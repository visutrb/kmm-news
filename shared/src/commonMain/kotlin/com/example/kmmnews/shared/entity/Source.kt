package com.example.kmmnews.shared.entity

import kotlinx.serialization.Serializable

@Serializable
data class Source(
    var id: String?,
    var name: String?
)