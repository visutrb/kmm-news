package com.example.kmmnews.shared.presentation

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

abstract class BasePresenter<View : BaseView>: CoroutineScope {

    override val coroutineContext: CoroutineContext = Dispatchers.Main

    protected var view: View? = null

    fun take(view: View) {
        this.view = view
    }
}