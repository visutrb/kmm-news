package com.example.kmmnews.shared.presentation.topheadlines

import com.example.kmmnews.shared.coroutines.dispatch
import com.example.kmmnews.shared.presentation.BasePresenter
import com.example.kmmnews.shared.service.NewsApiService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class TopHeadlinesPresenter : BasePresenter<TopHeadlinesView>() {

    private val newsApiService = NewsApiService()

    fun loadTopHeadlines() = dispatch {
        try {
            view?.onLoadingStarted()
            val topHeadlines = withContext(Dispatchers.Default) { newsApiService.getTopHeadlines() }
            view?.onLoadingFinished(topHeadlines)
        } catch (err: Throwable) {
            view?.onLoadingFailed(err)
        }
    }
}