package com.example.kmmnews.shared.presentation.topheadlines

import com.example.kmmnews.shared.entity.ArticlesResponse
import com.example.kmmnews.shared.presentation.BaseView

interface TopHeadlinesView : BaseView {
    fun onLoadingStarted()
    fun onLoadingFailed(error: Throwable? = null)
    fun onLoadingFinished(articlesResponse: ArticlesResponse)
}