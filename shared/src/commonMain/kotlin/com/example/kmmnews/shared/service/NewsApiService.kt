package com.example.kmmnews.shared.service

import com.example.kmmnews.shared.entity.ArticlesResponse
import io.ktor.client.*
import io.ktor.client.features.*
import io.ktor.client.features.json.*
import io.ktor.client.request.*

class NewsApiService {

    private val httpClient = HttpClient {
        install(JsonFeature)
    }

    @Throws(ClientRequestException::class, Throwable::class)
    suspend fun getTopHeadlines(): ArticlesResponse {
        val url = generateUrl(path = "/top-headlines", params = mapOf(Pair("country", "us")))
        try {
            return httpClient.get(url)
        } catch (requestException: ClientRequestException) {
            print("requestException = $requestException")
            throw requestException
        } catch (t: Throwable) {
            print("t = $t")
            throw t
        }
    }

    private fun generateUrl(
        path: String = "",
        params: Map<String, String> = HashMap()
    ): String {
        var url = BASE_URL + if (path.startsWith("/")) path else "/$path"
        params.forEach {
            val delimeter = getQueryDelimeter(url)
            url += "$delimeter${it.key}=${it.value}"
        }
        val delimeter = getQueryDelimeter(url)
        url += "${delimeter}apiKey=$API_KEY"
        return url
    }

    private fun getQueryDelimeter(url: String): String {
        return if (url.contains("?")) "&" else "?"
    }

    companion object {
        const val BASE_URL = "https://newsapi.org/v2"
        private const val API_KEY = "ca80497618ff4bd4af54c673db01ece8"
    }
}